#!/usr/bin/env python
#-*- coding: utf-8 -*-

from textblob import TextBlob
import os,sys

print "Simple Translator 1.0"
print
print "Aby zamknac program wpisz 'qt'"
print "\n"


while True:

    print "-> Podaj tresc do tlumaczenia:"

    try:

        textInput = raw_input().encode(sys.stdout.encoding)

        print

        if textInput == "qt":
            break

        przetlumaczony = str(TextBlob(str(textInput)).translate(to="pl"))

        print przetlumaczony.decode("utf-8")

    except:
        print "Nie powiodlo sie"

    print "\n"
